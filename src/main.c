#include "mem.h"
#include "mem_internals.h"
#include <stdbool.h>
#include <stdio.h>

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

bool test1_common_allocate(size_t heap_initial_size, size_t query){
    heap_init(heap_initial_size);
    int64_t* p = _malloc(query);
    if (p != NULL){
        return true;
    }
    return false;
}

bool test2_free_one_block(size_t heap_initial_size){
    heap_init(heap_initial_size);
    printf("\n");
    int64_t* p = _malloc(1000);
    struct block_header* block1 = block_get_header(p);
    if (!block1->is_free){
        printf("block 1 is allocated \n");
    }

    int_fast64_t* fast_64 = _malloc(sizeof(int_fast64_t));
    struct block_header* block2 = block_get_header(fast_64);
    if (!block2->is_free){
        printf("block 2 is allocated \n");
    }
    _free(fast_64);
    if (!block1->is_free && block2->is_free){
        _free(fast_64);
        return true;
    }
    return false;
}

bool test3_free_two_blocks(size_t heap_initial_size){
    heap_init(heap_initial_size);
    printf("\n");
    int64_t* p = _malloc(1000);
    struct block_header* block1 = block_get_header(p);
    if (!block1->is_free){
        printf("block 1 is allocated \n");
    }

    int_fast64_t* fast_64 = _malloc(sizeof(int_fast64_t));
    struct block_header* block2 = block_get_header(fast_64);
    if (!block2->is_free){
        printf("block 2 is allocated \n");
    }
    void* p3 = _malloc(123);
    struct block_header* block3 = block_get_header(p3);
    if (!block3->is_free){
        printf("block 3 is allocated \n");
    }
    _free(p);
    _free(p3);
    if (!block2->is_free && block1->is_free && block3->is_free){
        _free(fast_64);
        return true;
    }
    return false;
}

void test4_memory_ended_need_new(size_t heap_initial_size){
    heap_init(heap_initial_size);

    void* p  = _malloc(1000);
    struct block_header* block_1 = block_get_header(p);
    debug_heap(stdout,block_1);
    void* p2 = _malloc(10000);
    void* p3 = _malloc(100);
    debug_heap(stdout,block_1);
    if (p2 == NULL ){
        printf("failed to allocate 2 block\n");
        printf("Test 4 failed\n");
    }
    else{
        printf("Test 4 passed\n");
    }

    _free(p);
    _free(p2);
    _free(p3);
}


void test5_trying_to_allocate_in_occupied_memory(){
    heap_init(1);
    printf("Test 5: \n");
    void *p = _malloc(12000);
    struct block_header *block_1 = block_get_header(p);
    debug_heap(stdout, block_1);

    struct block_header * next_block = block_1->next;
    void*  off = next_block->contents + next_block->capacity.bytes;
    void*  tmp_pointer = mmap(off, 256, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20, -1, 0);

    if (tmp_pointer == NULL) {
        printf("Test 5 failed\n");
    }
    void *p2 = _malloc(4000);

    debug_heap(stdout, block_1);
    struct block_header *block_2 = block_get_header(p2);

    if (!block_1->is_free && !block_2->is_free) {
        _free(p);
        _free(p2);
        debug_heap(stdout, block_1);
    }
    printf("Test 5: Succes\n");
}

int main(){
    printf("Test 1 common allocate\n");
    if(test1_common_allocate(2000, sizeof(int64_t))){
        printf("Test 1 success\n");
    }
    else
        printf("Test 1 fail\n");

    printf("Test 2 free one block\n");
    if(test2_free_one_block(2000)){
        printf("Test 2 success\n");
    }
    else
        printf("Test 2 fail\n");

    printf("Test 3 free two block\n");
    if(test3_free_two_blocks(2000)){
        printf("Test 3 success\n");
    }
    else
        printf("Test 3 fail\n");

    printf("\n");
    printf("Test 4 heap memory ended need new\n");
    test4_memory_ended_need_new(1);
    test5_trying_to_allocate_in_occupied_memory();
}